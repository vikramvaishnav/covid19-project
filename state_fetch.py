import pandas as pd
import json
import urllib.request
import requests


class FetchDataOfNational:

    def national(self):
        response = requests.get("https://api.covid19india.org/data.json")
        print(response.status_code)
        self.jprint(response.json())

    def jprint(self, obj):
        # create a formatted string of the Python JSON object
        text = json.dumps(obj, sort_keys=True, indent=4)
        data = json.loads(text)
        # print(data['cases_time_series'][0])
        df = pd.DataFrame.from_dict(data['cases_time_series'])
        print(df)


class FetchDataOfState:

    def state_v1(self):
        response = requests.get("https://api.covid19india.org/state_district_wise.json")
        print(response.status_code)
        self.jprint(response.json())

    def state_v2(self):
        response = requests.get("https://api.covid19india.org/v2/state_district_wise.json")
        print(response.status_code)
        self.jprint(response.json())

    def jprint(self, obj):
        # create a formatted string of the Python JSON object
        text = json.dumps(obj, sort_keys=True, indent=4)
        data = json.loads(text)
        # print(data)
        df = pd.DataFrame.from_dict(data)
        print(df)

    def states_daily_confirmed_list(self, state_code):
        data = pd.read_csv("http://api.covid19india.org/states_daily_csv/confirmed.csv")
        state_data=[]
        df=data[state_code]
        date_df=data['date']
        for i in range(len(df)):
            if i!=0:
                state_data.append(state_data[i-1]+df[i])
            else:
                state_data.append(df[i])
        return state_data
    def date(self):
        data = pd.read_csv("http://api.covid19india.org/states_daily_csv/confirmed.csv")
        return data['date']


    def states_daily_deceased_list(self,state_code):
        data = pd.read_csv("https://api.covid19india.org/states_daily_csv/deceased.csv")
        state_data=[]
        df=data[state_code]
        
        for i in range(len(df)):
            if i!=0:
                state_data.append(state_data[i-1]+df[i])
            else:
                state_data.append(df[i])
        return state_data


    def states_daily_recovered_list(self,state_code):
        data = pd.read_csv("https://api.covid19india.org/states_daily_csv/recovered.csv")
        state_data=[]
        df=data[state_code]
        date_df=data['date']
        for i in range(len(df)):
            if i!=0:
                state_data.append(state_data[i-1]+df[i])
            else:
                state_data.append(df[i])
        return state_data
    def states_daily_confirmed_dict(self, state_code):
        data = pd.read_csv("http://api.covid19india.org/states_daily_csv/confirmed.csv")
        state_data=[]
        df=data[state_code]
        date_df=data['date']
        for i in range(len(df)):
            if i!=0:
                state_data.append({'date':date_df[i], 'value':state_data[i-1]['value']+df[i]})
            else:
                state_data.append({'date':date_df[i], 'value':df[i]})
        return state_data
    def states_daily_deceased_dict(self,state_code):
        data = pd.read_csv("https://api.covid19india.org/states_daily_csv/deceased.csv")
        state_data=[]
        df=data[state_code]
        date_df=data['date']
        for i in range(len(df)):
            if i!=0:
                state_data.append({'date':date_df[i], 'value':state_data[i-1]['value']+df[i]})
            else:
                state_data.append({'date':date_df[i], 'value':df[i]})
        return state_data
    def states_daily_recovered_dict(self,state_code):
        data = pd.read_csv("https://api.covid19india.org/states_daily_csv/recovered.csv")
        state_data=[]
        df=data[state_code]
        date_df=data['date']
        for i in range(len(df)):
            if i!=0:
                state_data.append({'date':date_df[i], 'value':state_data[i-1]['value']+df[i]})
            else:
                state_data.append({'date':date_df[i], 'value':df[i]})
        return state_data


class FetchDataOfDeathAndRecover:
    def DeathAndRecover(self):
        response = requests.get("https://api.covid19india.org/deaths_recoveries.json")
        print(response.status_code)
        self.jprint(response.json())

    def jprint(self, obj):
        text = json.dumps(obj, sort_keys=True, indent=4)
        data = json.loads(text)
        # print(data)
        df = pd.DataFrame.from_dict(data)
        print(df)


if __name__ == "__main__":
    f3 = FetchDataOfDeathAndRecover()
    f3.DeathAndRecover()
