from flask import render_template, url_for, flash, redirect ,request, jsonify
from tracker import app
import math
import numpy as np
# import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge
from sklearn.model_selection import GridSearchCV
import pandas as pd
from state_data import get_data, today_predict, tomorrow_predict, get_state_code
from national_data2 import get_data_national, today_predict_national, tomorrow_predict_national
import datetime
inf,rec,dth=[],[],[]
state_code=""
@app.route('/')   
@app.route('/home')
def home():
    today=today_predict_national()
    tom=tomorrow_predict_national()
    return render_template("home.html" , today_conf=int(today[0][3]), tom_conf=int(tom[0][3]), today_inf=int(today[0][0]), tom_inf=int(tom[0][0]), today_rec=int(today[0][1]), tom_rec=int(tom[0][1]), today_dth=int(today[0][2]), tom_dth=int(tom[0][2]))
@app.route('/states', methods=["POST","GET"])

def state():
    global state_code
    state_name=request.args['state']
    state_code= get_state_code(state_name)
    print(state_code)
    today=today_predict(state_code)
    tom=tomorrow_predict(state_code)
    print("tod",today)
    return render_template('index.html', today_conf=int(today[0][3]), tom_conf=int(tom[0][3]), today_inf=int(today[0][0]), tom_inf=int(tom[0][0]), today_rec=int(today[0][1]), tom_rec=int(tom[0][1]), today_dth=int(today[0][2]), tom_dth=int(tom[0][2]), state=state_name)
@app.route('/api/v1/resources/data/line', methods=['POST'])
def api_line():
    global state_code
    global inf,rec,dth
    inf,rec,dth,conf=[],[],[],[]
    inf,rec,dth,conf=get_data(state_code)
    #print(inf)
    return jsonify([{"inf":inf, "rec":rec, "dth":dth,"conf":conf}])
@app.route('/api/v1/resources/data/line_national', methods=['POST'])
def api_line_national():
    global inf,rec,dth
    inf,rec,dth,conf=[],[],[],[]
    inf,rec,dth,conf=get_data_national()
    print(dth)
    return jsonify([{"inf":inf, "rec":rec, "dth":dth,"conf":conf}])

