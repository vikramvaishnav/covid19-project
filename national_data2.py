import pandas as pd
import json
import urllib.request
import requests
import numpy as np
import datetime
from sklearn.linear_model import Ridge
from sklearn.model_selection import GridSearchCV
from state_fetch import FetchDataOfState
import datetime

population = 1380004385

df_dth = pd.read_csv("tracker/static/dataset/time_series_covid19_deaths_global.csv")
df_con = pd.read_csv("tracker/static/dataset/time_series_covid19_confirmed_global.csv")
df_rec = pd.read_csv("tracker/static/dataset/time_series_covid19_recovered_global.csv")

df_con = df_con.loc[df_con["Country/Region"] == "India"]
df_rec = df_rec.loc[df_rec["Country/Region"] == "India"]
df_dth = df_dth.loc[df_dth["Country/Region"] == "India"]

df_con = df_con.drop(["Province/State", "Country/Region", "Lat", "Long"], axis=1)
df_rec = df_rec.drop(["Province/State", "Country/Region", "Lat", "Long"], axis=1)
df_dth = df_dth.drop(["Province/State", "Country/Region", "Lat", "Long"], axis=1)

df_con = df_con.T
df_dth = df_dth.T
df_rec = df_rec.T
inf = []
rec = []
dth = []
conf = []

X_cml = df_con.values
X_cml = X_cml.reshape((1, -1))[0]
X_cml = X_cml[X_cml != 0]
# X_cml=X_cml[:-1]
# recovered = cumulative recovered cases
recovered = df_rec.values
recovered = recovered.reshape((1, -1))[0]
recovered = recovered[recovered != 0]
# recovered=recovered[:-1]
# death = cumulative deaths

death = df_dth.values
death = death.reshape((1, -1))[0]
death = death[death != 0]
# death=death[:-1]
recovered = recovered[len(recovered) - len(death):]
X_cml = X_cml[len(X_cml) - len(death):]
death, death_test = death[:-5], death[len(death) - 5:]
recovered, recovered_test = recovered[:-5], recovered[len(recovered) - 5:]
X_cml, X_cml_test = X_cml[:-5], X_cml[len(X_cml) - 5:]


def data_spilt(data, orders, start):
    x_train = np.empty((len(data) - start - orders, orders))
    y_train = data[start + orders:]

    for i in range(len(data) - start - orders):
        x_train[i] = data[i + start:start + orders + i]

    # Exclude the day (Feb. 12, 2020) of the change of the definition of confirmed cases in Hubei China.
    #     x_train = np.delete(x_train, np.s_[28 - (orders + 1) - start:28 - start], 0)
    #     y_train = np.delete(y_train, np.s_[28 - (orders + 1) - start:28 - start])

    return x_train, y_train


def ridge(x, y):
    print('\nStart searching good parameters for the task...')
    parameters = {'alpha': np.arange(0, 0.100005, 0.000005).tolist(),
                  "tol": [1e-8],
                  'fit_intercept': [True, False],
                  'normalize': [True, False]}

    clf = GridSearchCV(Ridge(), parameters, n_jobs=-1, cv=5)
    clf.fit(x, y)

    print('\nResults for the parameters grid search:')
    print('Model:', clf.best_estimator_)
    print('Score:', clf.best_score_)

    return clf


########## data preprocess ##########
def SIR(X_cml, recovered, death, population):
    global dates, base, inf, rec, dth, conf
    dates = []
    dates = list(df_dth.loc[(df_dth != 0).all(axis=1)].index)
    base = datetime.datetime.strptime(dates[-1], "%m/%d/%y")
    inf, rec, dth, conf = [], [], [], []
    X = X_cml - recovered - death
    R = recovered
    D = death

    n = np.array([population] * len(X), dtype=np.float64)

    S = n - X - R
    rho = 1 / 14

    X_diff = np.array([X[:-1], X[1:]], dtype=np.float64).T
    R_diff = np.array([R[:-1], R[1:]], dtype=np.float64).T
    D_diff = np.array([D[:-1], D[1:]], dtype=np.float64).T
    alpha = (D[1:] - D[:-1]) / (rho * X[:-1])
    gamma = (R[1:] - R[:-1]) / (X[:-1] * (1 - alpha))
    beta = n[:-1] * (X[1:] - X[:-1] + R[1:] - R[:-1] + D[1:] - D[:-1]) / (X[:-1] * (n[:-1] - X[:-1] - R[:-1] - D[:-1]))
    R0 = beta / gamma

    ########## Parameters for Ridge Regression ##########
    ##### Orders of the two FIR filters in (12), (13) in the paper. #####
    orders_beta = 3
    orders_gamma = 3
    orders_alpha = 3

    ##### Select a starting day for the data training in the ridge regression. #####
    start_beta = 10
    start_gamma = 10
    start_alpha = 10
    ########## Print Info ##########
    print("\nThe latest transmission rate beta of SIR model:", beta[-1])
    print("The latest recovering rate gamma of SIR model:", gamma[-1])
    print("The latest mortality rate alpha of SIR model:", alpha[-1])
    print("The latest basic reproduction number R0:", R0[-1])

    ########## Ridge Regression ##########
    ##### Split the data to the training set and testing set #####
    x_beta, y_beta = data_spilt(beta, orders_beta, start_beta)
    x_gamma, y_gamma = data_spilt(gamma, orders_gamma, start_gamma)
    x_alpha, y_alpha = data_spilt(alpha, orders_alpha, start_alpha)

    ##### Searching good parameters #####
    #     clf_beta = ridge(x_beta, y_beta)
    #     clf_gamma = ridge(x_gamma, y_gamma)

    #### Training and Testing #####
    clf_beta = Ridge(alpha=0.003765, copy_X=True, fit_intercept=False, max_iter=None, normalize=True, random_state=None,
                     solver='auto', tol=1e-08).fit(x_beta, y_beta)
    clf_gamma = Ridge(alpha=0.001675, copy_X=True, fit_intercept=False, max_iter=None, normalize=True,
                      random_state=None, solver='auto', tol=1e-08).fit(x_gamma, y_gamma)
    clf_alpha = Ridge(alpha=0.001675, copy_X=True, fit_intercept=False, max_iter=None, normalize=True,
                      random_state=None, solver='auto', tol=1e-08).fit(x_alpha, y_alpha)

    #     clf_beta= model(x_beta,y_beta)

    #     clf_gamma=model(x_gamma,y_gamma)
    beta_hat = clf_beta.predict(x_beta)
    #     beta_hat= clf_beta.predict(np.reshape(x_beta, (x_beta.shape[0], 1, x_beta.shape[1])))

    gamma_hat = clf_gamma.predict(x_gamma)
    alpha_hat = clf_alpha.predict(x_alpha)
    #     gamma_hat= clf_gamma.predict(np.reshape(x_gamma, (x_gamma.shape[0], 1, x_gamma.shape[1])))

    ##### Plot the training and testing results #####
    #     plt.figure(1)
    #     plt.plot(y_beta, label=r'$\beta (t)$')
    #     plt.plot(beta_hat, label=r'$\hat{\beta}(t)$')
    #     plt.legend()

    #     plt.figure(2)
    #     plt.plot(y_gamma, label=r'$\gamma (t)$')
    #     plt.plot(gamma_hat, label=r'$\hat{\gamma}(t)$')
    #     plt.legend()

    #     plt.figure(3)
    #     plt.plot(y_alpha, label=r'$\alpha (t)$')
    #     plt.plot(alpha_hat, label=r'$\hat{\alpha}(t)$')
    #     plt.legend()

    ########## Time-dependent SIR model ##########

    ##### Parameters for the Time-dependent SIR model #####
    stop_X = 0  # stopping criteria
    stop_day = 100  # maximum iteration days (W in the paper)

    day_count = 0
    turning_point = 0

    S_predict = [S[-1]]
    X_predict = [X[-1]]
    R_predict = [R[-1]]
    D_predict = [D[-1]]

    predict_beta = np.array(beta[-orders_beta:]).tolist()
    predict_gamma = np.array(gamma[-orders_gamma:]).tolist()
    predict_alpha = np.array(alpha[-orders_alpha:]).tolist()
    while (X_predict[-1] >= stop_X) and (day_count <= stop_day):
        if predict_beta[-1] > predict_gamma[-1]:
            turning_point += 1

        next_beta = clf_beta.predict(np.asarray([predict_beta[-orders_beta:]]))[0]
        #         next_beta = clf_beta.predict(np.reshape(np.asarray([predict_beta[-orders_beta:]]),(np.asarray([predict_beta[-orders_beta:]]).shape[0],1,np.asarray([predict_beta[-orders_beta:]]).shape[1])))[0]
        next_gamma = clf_gamma.predict(np.asarray([predict_gamma[-orders_gamma:]]))[0]
        #         next_gamma = clf_gamma.predict(np.reshape(np.asarray([predict_gamma[-orders_gamma:]]),(np.asarray([predict_gamma[-orders_gamma:]]).shape[0],1,np.asarray([predict_gamma[-orders_gamma:]]).shape[1])))[0]
        next_alpha = clf_alpha.predict(np.asarray([predict_alpha[-orders_alpha:]]))[0]
        if next_beta < 0:
            next_beta = 0
        if next_gamma < 0:
            next_gamma = 0
        if next_alpha < 0:
            next_alpha = 0

        predict_beta.append(next_beta)
        predict_gamma.append(next_gamma)
        predict_alpha.append(next_alpha)

        next_S = ((-predict_beta[-1] * S_predict[-1] *
                   X_predict[-1]) / n[-1]) + S_predict[-1]
        next_X = ((predict_beta[-1] * S_predict[-1] * X_predict[-1]) /
                  n[-1]) - ((1 - predict_alpha[-1]) * predict_gamma[-1] * X_predict[-1]) - (
                             predict_alpha[-1] * rho * X_predict[-1]) + X_predict[-1]
        next_R = ((1 - predict_alpha[-1]) * predict_gamma[-1] * X_predict[-1]) + R_predict[-1]
        next_D = (predict_alpha[-1] * rho * X_predict[-1]) + D_predict[-1]
        S_predict.append(next_S)
        X_predict.append(next_X)
        R_predict.append(next_R)
        D_predict.append(next_D)

        day_count += 1

    ########## Print Info ##########
    print('\nConfirmed cases tomorrow:', np.rint(X_predict[1] + R_predict[1] + D_predict[1]))
    print('Infected persons tomorrow:', np.rint(X_predict[1]))
    print('Recovered persons tomorrow:', np.rint(R_predict[1]))
    print('Death persons tomorrow:', np.rint(D_predict[1]))

    print('\nEnd day:', day_count)
    print('Confirmed cases on the end day:', np.rint(X_predict[-2] + R_predict[-2]))

    print('\nTuring point:', turning_point)

    ########## Plot the time evolution of the time-dependent SIR model ##########
    #     plt.figure(4)
    #     plt.plot(range(len(X) - 1, len(X) - 1 + len(X_predict)), X_predict, '*-', label=r'$\hat{X}(t)$', color='darkorange')
    #     plt.plot(range(len(X) - 1, len(X) - 1 + len(X_predict)), R_predict, '*-', label=r'$\hat{R}(t)$', color='limegreen')

    #     plt.plot(range(len(X)), X, 'o--', label=r'$X(t)$', color='chocolate')
    #     plt.plot(range(len(X)), R, 'o--', label=r'$R(t)$', color='darkgreen')

    #     plt.xlabel('Day')
    #     plt.ylabel('Person')
    #     plt.title('Time evolution of the time-dependent SIR model.')

    #     plt.legend()

    #     plt.figure(5)
    #     plt.plot(range(len(X) - 1, len(X) - 1 + len(X_predict)), D_predict, '*-', label=r'$\hat{D}(t)$', color='blue')
    #     plt.plot(range(len(X)), D, 'o--', label=r'$D(t)$', color='darkblue')
    #     plt.xlabel('Day')
    #     plt.ylabel('Person')
    #     plt.title('Death ')
    #     plt.show()
    # print(base)
    X = list(X)
    X.extend(X_predict[1:])
    # dates=[datetime.datetime.strftime(datetime.datetime.strptime(dates[i],"%m/%d/%y"),"%Y-%m-%d") for i in range(len(dates))]
    print(dates)
    date_list = [(base + datetime.timedelta(days=x)).strftime("%m/%d/%y") for x in range(len(X) - len(dates))]
    dates.extend(date_list)

    D = list(D)
    D.extend(D_predict[1:])
    R = list(R)
    R.extend(R_predict[1:])
    #  print("x",X)

    confl = list(np.array(X) + np.array(R) + np.array(D))
    dates = [datetime.datetime.strftime(datetime.datetime.strptime(dates[i], "%m/%d/%y"), "%Y-%m-%d") for i in
             range(len(dates))]
    for i in range(len(X)):
        inf.append({"date": dates[i], "value": float(round(X[i]))})

        rec.append({"date": dates[i], "value": float(round(R[i]))})
        dth.append({"date": dates[i], "value": float(round(D[i]))})

        conf.append({"date": dates[i], "value": float(round(confl[i]))})

    # print(inf)

    return X_predict, D_predict, R_predict


def get_data_national():
    SIR(X_cml, recovered, death, population)
    print(dth)
    return inf, rec, dth, conf


def today_predict_national():
    infe, rc, dh, con = get_data_national()

    data = [[i['value'], j['value'], k['value'], l['value']] for i, j, k, l in zip(infe, rc, dh, con) if
            i['date'] == datetime.datetime.strftime(datetime.datetime.date(datetime.datetime.now()), "%Y-%m-%d")]

    return data


def tomorrow_predict_national():
    infe, rc, dh, con = get_data_national()
    data = [[i['value'], j['value'], k['value'], l['value']] for i, j, k, l in zip(infe, rc, dh, con) if
            i['date'] == datetime.datetime.strftime(
                datetime.datetime.date(datetime.datetime.now() + datetime.timedelta(days=1)), "%Y-%m-%d")]

    return data
